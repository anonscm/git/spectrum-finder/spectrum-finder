# the name of the target operating system
# cmake -DCMAKE_TOOLCHAIN_FILE=./toolchain-condor.cmake -DCMAKE_INSTALL_PREFIX=./condor .
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_VERSION 1)

# which compilers to use for C and C++
SET(CMAKE_C_COMPILER /home/langella/developpement/git/gpf/condor/condor_compile_gcc)
SET(CMAKE_CXX_COMPILER /home/langella/developpement/git/gpf/condor/condor_compile_g++)

#SET(CMAKE_RC_COMPILER i586-mingw32msvc-windres)

# here is the target environment located
#SET(CMAKE_FIND_ROOT_PATH  /usr/i586-mingw32msvc /usr/local/opt/qt)

# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
#set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
#set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)