/**
 * \file mzxmlSpectrumParser.cpp
 * \date June, 14 2011
 * \author Edlira Nano
 */

#include "mzxmlSpectrumParser.h"
#include "../encode/decodeBinary.h"
#include "../spectrum/spectrum.h"

MzxmlSpectrumParser::MzxmlSpectrumParser(SpectrumReaderBase &reader) :
		XmlSpectrumParser(reader) {
	_zlib_compression = false;
	_compressed_len = 0;
}

MzxmlSpectrumParser::~MzxmlSpectrumParser() {
	_zlib_compression = false;
	_compressed_len = 0;
}

bool MzxmlSpectrumParser::startElement(const QString & /* namespaceURI */,
		const QString & /* localName */, const QString & qName,
		const QXmlAttributes & attributes) {
	bool is_ok = true;
	if (qName == "scan") {
		is_ok = startElement_scan(attributes);
	}

	else if (qName == "peaks" && _ms_level == 2) {
		is_ok = startElement_peaks(attributes);
	}

	else if (qName == "precursorMz" && _ms_level == 2) {
		is_ok = startElement_precursorMz(attributes);
	}

	_currentText.clear();
	return is_ok;
}

bool MzxmlSpectrumParser::endElement(const QString & /* namespaceURI */,
		const QString & /* localName */, const QString & qName) {
	if (qName == "peaks" && _ms_level == 2) {

		if (_peakscount != 0) {
			endElement_peaks();
		} else {
			// skip this scan
			qDebug() << "scan number " << _current_scan_number
					<< " have peakscount = 0 so we skipped the scan";
		}
	} else if (qName == "precursorMz" && _ms_level == 2) {
		endElement_precursorMz();
	}
	return true;
}

bool MzxmlSpectrumParser::startElement_scan(const QXmlAttributes & attributes) {
	QString ms_level = attributes.value("msLevel");
	if (ms_level.isEmpty()) {
		_errorStr = QObject::tr(
				"the msLevel attribute in mzXML file must not be empty\n");
		return false;
	}
	_ms_level = ms_level.toInt();

	if (_ms_level == 2) {
		_spectrum.clear();

		_current_scan_number = attributes.value("num").toInt();
		_spectrum.setSpectrumId(
				SpectrumId(_current_scan_number, _reader.getMsRun()));

		QString temp = attributes.value("retentionTime").mid(2,
				(attributes.value("retentionTime").size() - 3));
		_retention_time = temp.toDouble();

		QString peaks_count = attributes.value("peaksCount");
		if (peaks_count.isEmpty()) {
			_errorStr =
					QObject::tr(
							"the peaksCount attribute in mzXML file must not be empty\n");
			return false;
		}
		_peakscount = peaks_count.toInt();

		QString lowMz = attributes.value("lowMz");
		QString highMz = attributes.value("highMz");

		if (lowMz.isEmpty() || highMz.isEmpty()) {
			_scan_type = "Full";
		} else {
			_low_mz = lowMz.toDouble();
			_high_mz = highMz.toDouble();
			if ((_high_mz - _low_mz) > 100)
				_scan_type = "Full";
			else
				_scan_type = "Zoom";
		}
	}
	return true;
}

bool MzxmlSpectrumParser::startElement_peaks(
		const QXmlAttributes & attributes) {
	/*
	 *
	 pairOrder="m/z-int"
	 */
	QString precision = attributes.value("precision");
	if (precision.isEmpty())
		_current_binary_precision = 32;
	else
		_current_binary_precision = precision.toInt();

	//byteOrder="network"
	_is_bigendian = true;
	QString byteOrder = attributes.value("byteOrder");
	if (byteOrder.isEmpty()) {
		_is_bigendian = true;
	} else {
		if (byteOrder != "network") {
			_is_bigendian = false;
		}else{
			_is_bigendian = true;
		}
	}

	//compressionType="zlib"
	QString compressionType = attributes.value("compressionType");
	if (compressionType.isEmpty() ||
		     compressionType != "zlib") {
		_zlib_compression = false;
	} else {
		_zlib_compression = true;
	}

	//    compressedLen="339"
	QString compressedLen = attributes.value("compressedLen");
	if (compressedLen.isEmpty()) {
		this->_compressed_len = 0;
	} else {
		_compressed_len = compressedLen.toInt();
	}

	return true;
}

bool MzxmlSpectrumParser::endElement_peaks() {
	const QByteArray encoded_data = _currentText.toAscii();

	std::vector<mcq_double> decode = DecodeBinary::base64_decode_peaks(
			encoded_data, _current_binary_precision, _is_bigendian,
			this->_peakscount, this->_zlib_compression, this->_compressed_len);
	//qDebug("Decode time is %d ms", tdecode.elapsed());
	// verify that _peakscount is the same as the size of the decoded
	// peaks vector

	unsigned int decoded_pairs = decode.size() / 2;
	if (_peakscount == decoded_pairs) {

		_spectrum.reserve(decoded_pairs);

		//qDebug() << "Spectrum decoded values (pairs of mz int)\n";
		//qDebug() << "Spectrum mz count = " << decoded_data.size() / 2;

		for (unsigned int i = 0; i < decode.size(); i += 2) {
			// i = mz, i+1 = intensity
			//qDebug() << "mz = " << decoded_data[i];
			_spectrum.push_back(mz_int(decode[i], decode[i + 1]));
		}
		_reader.spectrumEvent(_spectrum);

	} else {
		// skip this scan
		qDebug() << "MzxmlSpectrumParser : problem in scan number "
				<< _current_scan_number << " attribute peaksCount is "
				<< _peakscount << ", but we decoded " << decoded_pairs
				<< " peaks. Skipping scan.\n";
	}
	return true;
}

bool MzxmlSpectrumParser::startElement_precursorMz(
		const QXmlAttributes & attributes) {
	QString precursorInt = attributes.value("precursorIntensity");
	if (precursorInt.isEmpty()) {
		precursorInt = "0";
	}
	_spectrum.setPrecursorInt(precursorInt.toDouble());

	QString precursorZ = attributes.value("precursorCharge");
	if (precursorZ.isEmpty()) {
		_spectrum.setPrecursorZ(-1);
	} else {
		_spectrum.setPrecursorZ(precursorZ.toInt());
	}
	return true;
}

bool MzxmlSpectrumParser::endElement_precursorMz() {
	if (_currentText.isEmpty()) {
		_errorStr = QObject::tr(
				"the precursorMz element in mzXML file must not be empty\n");
		return false;
	}
	_spectrum.setPrecursorMz(_currentText.toDouble());
	return true;
}

