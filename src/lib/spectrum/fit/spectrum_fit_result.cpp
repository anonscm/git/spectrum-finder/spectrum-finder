/*
 * spectrum_fit_result.cpp
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#include "spectrum_fit_result.h"
#include "spectrum_fit_algo.h"

SpectrumFitResult::SpectrumFitResult() {
	_peak_count = 0;
	_is_consitent = false;
}

SpectrumFitResult::~SpectrumFitResult() {
	// TODO Auto-generated destructor stub
}

void SpectrumFitResult::getSpectrumResultMap(
		std::map<QString, QString> &toFill) const {
	QString temp();
	toFill.insert(pair<QString, QString>("Peak count", QString().setNum(_peak_count,10)));
	toFill.insert(
			pair<QString, QString>("Cosinus score", QString().setNum(_cosinus_score,'g',6)));
	toFill.insert(
			pair<QString, QString>("Coefficient correlation",
					QString().setNum(_correlation_coefficient,'g',6)));
	toFill.insert(
			pair<QString, QString>("Pearson probability",
					QString().setNum(_pearson_probability,'g',6)));
}
