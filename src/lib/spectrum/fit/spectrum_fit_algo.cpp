/*
 * spectrum_fit_algo.cpp
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#include "spectrum_fit_algo.h"

SpectrumFitAlgo::SpectrumFitAlgo(const GpfParams &gpf_params):
_ppm_precision(gpf_params.getPpmPrecision()){
}

SpectrumFitAlgo::~SpectrumFitAlgo() {
	// TODO Auto-generated destructor stub
}

void SpectrumFitAlgo::getIntensityMatchedPairs(const Spectrum & ref,
		const Spectrum & toCompare,
		vector<pair<mcq_double, mcq_double> >& toFill) const{

	Spectrum::const_iterator refpeak(ref.begin());
	Spectrum::const_iterator comparepeak(toCompare.begin());
	mcq_double delta;
	mcq_double min;
	mcq_double max;

	for (; comparepeak != toCompare.end(); comparepeak++) {
		delta = (comparepeak->first * _ppm_precision) / 1000000;
		min = comparepeak->first - delta;
		max = comparepeak->first + delta;
		for (; refpeak != ref.end(); refpeak++) {
			if ((refpeak->first > min) & (refpeak->first < max)) {
				//toFill.addPeak();
				toFill.push_back(
						pair<mcq_double, mcq_double>(comparepeak->second,
								refpeak->second));
			}
		}
		refpeak = ref.begin();
	}

}
