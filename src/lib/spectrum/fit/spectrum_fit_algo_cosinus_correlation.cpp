/*
 * spectrum_fit_algo_cosinus_correlation.cpp
 *
 *  Created on: 18 avr. 2012
 *      Author: valot
 */

#include "spectrum_fit_algo_cosinus_correlation.h"

SpectrumFitAlgoCosinusCorrelation::SpectrumFitAlgoCosinusCorrelation(
		const GpfParams &gpf_params) :
		SpectrumFitAlgo(gpf_params),
		_algo_correlation(gpf_params),
		_algo_cosinus(gpf_params)
{
}

SpectrumFitAlgoCosinusCorrelation::~SpectrumFitAlgoCosinusCorrelation() {
	// TODO Auto-generated destructor stub
}

void SpectrumFitAlgoCosinusCorrelation::compareSpectrum2Spectrum(const Spectrum & ref,
		const Spectrum & toCompare, SpectrumFitResult &toFill) const {

	_algo_cosinus.compareSpectrum2Spectrum(ref, toCompare, toFill);

	if (toFill.isConsistent()) {
		_algo_correlation.compareSpectrum2Spectrum(ref, toCompare, toFill);
	}
}
