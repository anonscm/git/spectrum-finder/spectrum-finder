/*
 * spectrum_fit_algo_basic.h
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#ifndef SPECTRUM_FIT_ALGO_CORRELATION_H_
#define SPECTRUM_FIT_ALGO_CORRELATION_H_

#include "spectrum_fit_algo.h"

class SpectrumFitAlgoCorrelation: public SpectrumFitAlgo {
public:
	SpectrumFitAlgoCorrelation(const GpfParams &gpf_params);
	virtual ~SpectrumFitAlgoCorrelation();
	void compareSpectrum2Spectrum(const Spectrum & ref,
			const Spectrum & toCompare, SpectrumFitResult &toFill) const;

private:
	mcq_double calculated_correlation(
			std::vector<pair <mcq_double, mcq_double> > &matched_int) const;
	mcq_double calculated_slope(
			std::vector<pair <mcq_double, mcq_double> > &matched_int) const;
	const int _min_number_of_peak_to_score;
	const mcq_double _max_pearson_correlation_limit;
};

#endif /* SPECTRUM_FIT_ALGO_CORRELATION_H_ */
