/*
 * spectrum_fit_algo_basic.cpp
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#include "spectrum_fit_algo_correlation.h"
#include "../../../alglib/statistics.h"
#include <vector>
#include <cmath>
#include <QDebug>

SpectrumFitAlgoCorrelation::SpectrumFitAlgoCorrelation(const GpfParams &gpf_params) :
SpectrumFitAlgo(gpf_params),
_min_number_of_peak_to_score(gpf_params.getMinNumberOfPeakToScore()),
_max_pearson_correlation_limit(gpf_params.getMaxPvalue())
{
}

SpectrumFitAlgoCorrelation::~SpectrumFitAlgoCorrelation() {
	// TODO Auto-generated destructor stub
}

void SpectrumFitAlgoCorrelation::compareSpectrum2Spectrum(const Spectrum & ref,
		const Spectrum & toCompare, SpectrumFitResult &toFill) const {

	//get matched peak
	vector<pair<mcq_double, mcq_double> > matched_int;
	this->getIntensityMatchedPairs(ref, toCompare, matched_int);

	toFill.setPeakCount(matched_int.size());
	toFill.setConsistent(false);

	//If peak number is correct and pValue less than limit, result is consistant
	if (toFill.getPeakCount() > _min_number_of_peak_to_score) {
		toFill.setCorrelationCoefficient(
				this->calculated_correlation(matched_int));
		mcq_double tail, left, rigth;
		alglib::pearsoncorrelationsignificance(
				toFill.getCorrelationCoefficient(), toFill.getPeakCount(), tail,
				left, rigth);
		toFill.setPearsonProbability(tail);

		if (tail < _max_pearson_correlation_limit) {
			toFill.setConsistent(true);
		}
	}
}

mcq_double SpectrumFitAlgoCorrelation::calculated_correlation(
		std::vector<pair<mcq_double, mcq_double> > &matched_int) const {
	mcq_double moyX = 0;
	mcq_double moyY = 0;
	mcq_double moysumX = 0;
	mcq_double moysumY = 0;
	mcq_double moysumxy = 0;
	mcq_double R = 0;
	std::vector<pair<mcq_double, mcq_double> >::const_iterator map_iterated(
			matched_int.begin());

	int matched(0);
	//calculated mean
	for (; map_iterated != matched_int.end(); map_iterated++) {
		moyX += map_iterated->first;
		moyY += map_iterated->second;
		matched++;
	}
	moyX = moyX / ((mcq_double) matched);
	moyY = moyY / ((mcq_double) matched);

	//calculated R2
	map_iterated = matched_int.begin();
	for (; map_iterated != matched_int.end(); map_iterated++) {
		moysumX += (moyX - map_iterated->first) * (moyX - map_iterated->first);
		moysumY += (moyY - map_iterated->second)
				* (moyY - map_iterated->second);
		moysumxy += (moyX - map_iterated->first)
				* (moyY - map_iterated->second);
	}
	R = moysumxy / (sqrt(moysumX) * sqrt(moysumY));

	return (R);
}

mcq_double SpectrumFitAlgoCorrelation::calculated_slope(
		std::vector<pair<mcq_double, mcq_double> > &matched_int) const {
	mcq_double moyX(0);
	mcq_double moyY(0);
	mcq_double valint1(0);
	mcq_double valint2(0);

	std::vector<pair<mcq_double, mcq_double> >::const_iterator map_iterated(
			matched_int.begin());

	int matched(0);
	//calculated mean
	for (; map_iterated != matched_int.end(); map_iterated++) {
		moyX += map_iterated->first;
		moyY += map_iterated->second;
		matched++;
	}
	moyX = moyX / ((mcq_double) matched);
	moyY = moyY / ((mcq_double) matched);

	//calculated b
	map_iterated = matched_int.begin();
	for (; map_iterated != matched_int.end(); map_iterated++) {
		valint1 += map_iterated->second * (map_iterated->first - moyX);
		valint2 += (map_iterated->first - moyX) * (map_iterated->first - moyX);
	}
	return (valint1 / valint2);

}
