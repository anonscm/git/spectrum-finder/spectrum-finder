/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file spectrum.h
 * \date 18 janv. 2010
 * \author Olivier Langella
 */

#ifndef SPECTRUM_H_
#define SPECTRUM_H_ 1

#include <iostream>

#include <QDataStream>
#include <map>
#include <vector>

#include "../../config.h"
#include "spectrum_id.h"

using namespace std;

typedef pair<mcq_double, mcq_double> mz_int;

/**
 \class spectrum
 \brief Represents a MS spectrum (intensity/mz)
 */
class Spectrum {

public:
	Spectrum();
	Spectrum(const SpectrumId & spectrum_id);
	Spectrum(const Spectrum & toCopy);
	virtual ~Spectrum();

	typedef std::vector<mz_int>::iterator iterator;
	typedef std::vector<mz_int>::const_iterator const_iterator;

	iterator begin() {
		return _v_peaks.begin();
	}

	const_iterator begin() const {
		return _v_peaks.begin();
	}

	iterator end() {
		return _v_peaks.end();
	}

	const_iterator end() const {
		return _v_peaks.end();
	}

	void setPeaks(const mcq_double mz, const mcq_double intensity);

	void setSpectrumId(const SpectrumId & spectrum_id) {
		_spectrum_id = spectrum_id;
	}

	const SpectrumId & getSpectrumId() const {
		return (_spectrum_id);
	}

	void setPrecursorMz(const mcq_double mz) {
		_precursor_mz = mz;
	}

	mcq_double getPrecursorMz() const {
		return (_precursor_mz);
	}

	void setPrecursorZ(const int z) {
		_precursor_z = z;
	}

	int getPrecursorZ() const {
		return (_precursor_z);
	}

	void setPrecursorInt(const mcq_double intens) {
		_precursor_int = intens;
	}

	mcq_double getPrecursorInt() const {
		return (_precursor_int);
	}

	mcq_double getPrecursorMh() const;

	void setPeakMz(const mcq_double mz);

	void setPeakIntensity(const mcq_double intensity);

	mcq_double getMaxIntensity() const;

	const unsigned int getSpectrumSize() const;

	void debugPrintValues() const;

	void reserve(const unsigned int size);

	void clear();

	void takeNmostIntense(int n, const Spectrum & toCopy);
	void applyCutOff(mcq_double mz_min, const Spectrum & toCopy);
	void cleanPeakIntensity(float percentIntMIn, const Spectrum & toCopy);

	void push_back(const mz_int& mz_int_pair);
private:
	void copyMembers(const Spectrum & toCopy);

	//pair of mz intensity
	std::vector<mz_int> _v_peaks;

	mcq_double _precursor_mz;

	int _precursor_z;

	mcq_double _precursor_int;

	SpectrumId _spectrum_id;
};

#endif /*SPECTRUM_H_*/
