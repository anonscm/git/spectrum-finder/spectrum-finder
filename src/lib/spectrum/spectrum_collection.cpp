/*
 * spectrum_collection.cpp
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#include "spectrum_collection.h"

SpectrumCollection::SpectrumCollection() {
	// TODO Auto-generated constructor stub

}

SpectrumCollection::~SpectrumCollection() {
	_spectrumList.clear();
}

void SpectrumCollection::push_back(const Spectrum& x) {
	_spectrumList.push_back(x);
}
