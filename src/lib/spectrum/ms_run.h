/*
 * ms_run.h
 *
 *  Created on: 17 avr. 2012
 *      Author: valot
 */

#ifndef MS_RUN_H_
#define MS_RUN_H_

#include <QFileInfo>
#include <QString>
#include <QDebug>

class SpectrumReaderBase;

class MsRun {
public:
	MsRun(const QFileInfo &fileinf);
	virtual ~MsRun();
	QString getAbsoluteFilePath() const{
		return(_fileinf.absoluteFilePath());
	}
	QString getFileName() const{
		return(_fileinf.fileName());
	}

	void readSpectrum(SpectrumReaderBase &reader);

    bool operator <(const MsRun& msrun) const
    {
        return(_fileinf.absoluteFilePath()<msrun._fileinf.absoluteFilePath());
    }
    bool operator ==(const MsRun& msrun) const
    {
        return(_fileinf.absoluteFilePath()==msrun._fileinf.absoluteFilePath());
    }
private:
	const QFileInfo _fileinf;
};

#endif /* MS_RUN_H_ */
