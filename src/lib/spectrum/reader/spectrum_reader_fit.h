/*
 * spectrum_reader_fit.h
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#ifndef SPECTRUM_READER_FIT_H_
#define SPECTRUM_READER_FIT_H_

#include "spectrum_reader_filter.h"
#include "../spectrum_collection.h"
#include "../../gpf_params.h"
#include "../fit/spectrum_fit_algo_cosinus_correlation.h"

class SpectrumReaderFit: public SpectrumReaderFilter {
public:
	SpectrumReaderFit(const GpfParams &gpf_params,
			const SpectrumCollection &spectrum_collection);
	virtual ~SpectrumReaderFit();

	void spectrumEvent(const Spectrum & spectrum);

private:
	const SpectrumCollection &_spectrum_collection;
	SpectrumFitAlgoCosinusCorrelation _algo_fit;
};

#endif /* SPECTRUM_READER_FIT_H_ */
