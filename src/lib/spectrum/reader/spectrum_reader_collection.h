/*
 * spectrum_reader_collection.h
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#ifndef SPECTRUM_READER_COLLECTION_H_
#define SPECTRUM_READER_COLLECTION_H_

#include "spectrum_reader_filter.h"
#include "../spectrum_collection.h"
#include "../../gpf_params.h"
#include <set>

class SpectrumReaderCollection: public SpectrumReaderFilter, public SpectrumCollection {
public:
	SpectrumReaderCollection(const GpfParams &gpf_params);
	virtual ~SpectrumReaderCollection();

	void addSpectrumId(SpectrumId & spectrum_id);

	void spectrumEvent(const Spectrum & spectrum);

private :

	set<SpectrumId> _spectrum_id_list;
};

#endif /* SPECTRUM_READER_COLLECTION_H_ */
