/*
 * spectrum_reader_filter.h
 *
 *  Created on: 13 avr. 2012
 *      Author: valot
 */

#ifndef SPECTRUM_READER_FILTER_H_
#define SPECTRUM_READER_FILTER_H_

#include "spectrum_reader_base.h"
#include "../../gpf_params.h"

class SpectrumReaderFilter: public SpectrumReaderBase {
public:
	SpectrumReaderFilter(const GpfParams &gpf_params);
	virtual ~SpectrumReaderFilter();
protected:
	void filterSpectrum(Spectrum &spectrum_result,
			const Spectrum &spectrum_to_filter) const;
private:
	const int _n_most_intense;
	const mcq_double mz_min;
	const float percentIntMIn;
};

#endif /* SPECTRUM_READER_FILTER_H_ */
