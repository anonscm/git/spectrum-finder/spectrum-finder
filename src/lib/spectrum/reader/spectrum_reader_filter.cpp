/*
 * spectrum_reader_filter.cpp
 *
 *  Created on: 13 avr. 2012
 *      Author: valot
 */

#include "spectrum_reader_filter.h"

SpectrumReaderFilter::SpectrumReaderFilter(const GpfParams &gpf_params) :
	SpectrumReaderBase(),
	_n_most_intense(gpf_params.getMostIntense()),
	mz_min(gpf_params.getMzMin()),
	percentIntMIn(gpf_params.getPercentIntMIn())
{
}

SpectrumReaderFilter::~SpectrumReaderFilter() {
	// TODO Auto-generated destructor stub
}

void SpectrumReaderFilter::filterSpectrum(Spectrum &spectrum_result,
		const Spectrum &spectrum_to_filter) const {
	Spectrum filter1, filter2;
	filter1.applyCutOff(mz_min,spectrum_to_filter);
	filter2.cleanPeakIntensity(percentIntMIn,filter1);
	spectrum_result.takeNmostIntense(_n_most_intense,filter2);
}
