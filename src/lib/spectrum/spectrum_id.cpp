/*
 * spectrum_id.cpp
 *
 *  Created on: 14 avr. 2012
 *      Author: olivier
 */

#include "spectrum_id.h"

SpectrumId::SpectrumId(int scan_num, MsRun* msrun) :
		_scan(scan_num),
		_p_msrun(msrun),
		_spectrum_info("")
{
}

SpectrumId::SpectrumId(const SpectrumId & toCopy) :
		_scan(toCopy._scan), _p_msrun(toCopy._p_msrun), _spectrum_info(
				toCopy._spectrum_info) {
}

SpectrumId::~SpectrumId() {
	// TODO Auto-generated destructor stub
}

