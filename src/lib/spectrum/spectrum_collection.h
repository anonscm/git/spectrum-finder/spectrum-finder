/*
 * spectrum_collection.h
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#ifndef SPECTRUM_COLLECTION_H_
#define SPECTRUM_COLLECTION_H_

#include <list>
#include "spectrum.h"

using namespace std;

class SpectrumCollection {
public:
	SpectrumCollection();
	virtual ~SpectrumCollection();

public:

	typedef std::list<Spectrum>::iterator iterator;
	typedef std::list<Spectrum>::const_iterator const_iterator;

	iterator begin() {
		return _spectrumList.begin();
	}

	const_iterator begin() const {
		return _spectrumList.begin();
	}

	iterator end() {
		return _spectrumList.end();
	}

	const_iterator end() const {
		return _spectrumList.end();
	}

	void push_back(const Spectrum& x);
private:
	list<Spectrum> _spectrumList;
};

#endif /* SPECTRUM_COLLECTION_H_ */
