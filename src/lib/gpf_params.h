/*
 * gpf_params.h
 *
 *  Created on: 11 avr. 2012
 *      Author: valot
 */

#ifndef GPF_PARAMS_H_
#define GPF_PARAMS_H_

#include "gpf_types.h"

class GpfParams {
public:
	GpfParams();
	virtual ~GpfParams();

    mcq_double getMaxPvalue() const {
		return _max_pvalue;
	}

	void setMaxPvalue(mcq_double maxPvalue) {
		_max_pvalue = maxPvalue;
	}

	mcq_double getMinCosinus() const {
		return _min_cosinus;
	}

	void setMinCosinus(mcq_double minCosinus) {
		_min_cosinus = minCosinus;
	}

	mcq_double getMzMin() const
    {
        return _mz_min;
    }

    void setMzMin(mcq_double mzMin)
    {
        _mz_min = mzMin;
    }

    float getPercentIntMIn() const
    {
        return _percentIntMIn;
    }

    void setPercentIntMIn(float percentIntMIn)
    {
        _percentIntMIn = percentIntMIn;
    }

    int getMinNumberOfPeakToScore() const
    {
        return _min_number_of_peak_to_score;
    }

    void setMinNumberOfPeakToScore(int minNumberOfPeakToScore)
    {
        _min_number_of_peak_to_score = minNumberOfPeakToScore;
    }

    mcq_double getPpmPrecision() const
    {
        return _ppm_precision;
    }

    void setPpmPrecision(mcq_double ppmPrecision)
    {
        _ppm_precision = ppmPrecision;
    }

    int getMostIntense() const
    {
        return _n_most_intense;
    }

    void setMostIntense(int mostIntense)
    {
        _n_most_intense = mostIntense;
    }

private:
    int _n_most_intense;
    mcq_double _ppm_precision;
    int _min_number_of_peak_to_score;
    mcq_double _mz_min;
    float _percentIntMIn;
    mcq_double _min_cosinus;
    mcq_double _max_pvalue;
};

#endif /* GPF_PARAMS_H_ */
