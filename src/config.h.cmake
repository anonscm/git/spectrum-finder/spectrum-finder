#ifndef _CONFIG_H
#define _CONFIG_H

#cmakedefine GPF_VERSION "@GPF_VERSION@"
#cmakedefine GPF_XSD "@GPF_XSD@"
#cmakedefine GPF_SCHEMA_VERSION "@GPF_SCHEMA_VERSION@"
#cmakedefine GPF_SCHEMA_FILE "@GPF_SCHEMA_FILE@"
#cmakedefine GPF_XSD_LOCATION_DIR "@GPF_XSD_LOCATION_DIR@"
#define QT_V_4_5  0x040500
#define QT_V_4_6  0x040600

#if (WIN32)
# define isnan(x) ((x) != (x))
#endif

#include <QDebug>

#include "lib/gpf_types.h"


#endif /* _CONFIG_H */
